import pysftp

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None  

password = 'secret'

with pysftp.Connection('svv-gftp.aod.no', username='ftp_bym', password=password, cnopts=cnopts) as sftp:
    sftp.chdir("ftp_bym:" + password)
    sftp.get("oslo", callback=lambda x,y: print("Downloaded: {} mb. {} %".format(round(x/1000000, 2),round(x*100/y, 2))), preserve_mtime=False)         # get the remote file
sftp.close()