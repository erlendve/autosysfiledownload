Dette programmet laster ned filen 'oslo' fra sftp serveren til autosys.


Før programmet kjøres må avhengigheter installeres; kjør filen "requirements.install.bat" som installerer pysftp pakken.


Gå inn i filen 'autosysfiledownload.py' og bytt ut verdien av variabelen 'secret' med passordet for å koble seg på serveren.

Deretter kjøres programmet ved kjøre autosysfiledownload.py med python.